package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/megatano/megatano_api/database"
	"github.com/megatano/megatano_api/router"
)

func main() {
	// Start a new fiber app
	app := fiber.New()
	database.ConnectDB()

	router.SetupRoutes(app)

	// Listen on PORT 8080
	app.Listen(":8080")
}
