package router

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	authRoutes "github.com/megatano/megatano_api/internals/routes/auth"
	noteRoutes "github.com/megatano/megatano_api/internals/routes/note"
)

func SetupRoutes(app *fiber.App) {
	api := app.Group("/api", logger.New())

	// Setup the Node Routes
	noteRoutes.SetupNoteRoutes(api)
	authRoutes.SetupNoteRoutes(api)
}
