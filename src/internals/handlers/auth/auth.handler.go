package authHandler

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"github.com/megatano/megatano_api/database"
	"github.com/megatano/megatano_api/internals/model"
	"golang.org/x/crypto/bcrypt"
)

func Login(c *fiber.Ctx) error {
	// db := database.DB
	auth := struct {
		Username string `json:"username" xml:"username" form:"username"`
		Password string `json:"password" xml:"password" form:"password"`
	}{}
	err := c.BodyParser(&auth)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	if auth.Username != "john" || auth.Password != "doe" {
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	t, err := accessToken(nil)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(fiber.Map{"token": t})
}

func Signup(c *fiber.Ctx) error {
	db := database.DB
	user := new(model.User)

	err := c.BodyParser(user)
	if err != nil || user.ConfirmPassword != user.Password {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	user.Password, _ = HashPassword(user.Password)
	user.ID = uuid.New()
	err = db.Create(&user).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create user", "data": err})
	}

	token, err := accessToken(user)

	return c.JSON(fiber.Map{"status": "success", "message": "Created User", "data": token})
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func accessToken(user *model.User) (string, error) {
	claims := jwt.MapClaims{
		"fullname": user.FullName,
		"role":     user.Role,
		"username": user.Username,
		"exp":      time.Now().Add(time.Hour * 72).Unix(),
		"iat":      time.Now().Unix(),
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	return t, err
}
