package authRoutes

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	authHandler "github.com/megatano/megatano_api/internals/handlers/auth"
)

func AuthorizationRequired() fiber.Handler {
	return jwtware.New(jwtware.Config{
		ErrorHandler:  AuthError,
		SigningKey:    []byte("secret"),
		SigningMethod: "HS256",
		AuthScheme:    "Bearer",
	})
}
func AuthError(c *fiber.Ctx, e error) error {
	c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
		"error": "Unauthorized",
		"msg":   e.Error(),
	})
	return nil
}

func SetupNoteRoutes(router fiber.Router) {
	auth := router.Group("/auth")
	// Log in
	auth.Post("/login", authHandler.Login)
	// Sign up
	auth.Post("/signup", authHandler.Signup)
	// Read all Notes
	// auth.Get("/", func(c *fiber.Ctx) error {})
	// Read one Note
	// note.Get("/:noteId", func(c *fiber.Ctx) error {})
	// Update one Note
	// note.Put("/:noteId", func(c *fiber.Ctx) error {})
	// Delete one Note
	// note.Delete("/:noteId", func(c *fiber.Ctx) error {})
}
