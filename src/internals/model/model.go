package model

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Note struct {
	gorm.Model           // Adds some metadata fields to the table
	ID         uuid.UUID `gorm:"type:uuid"` // Explicitly specify the type to be uuid
	Title      string    `json:"title" xml:"title" form:"title"`
	SubTitle   string    `json:"subTitle" xml:"subTitle" form:"subTitle"`
	Text       string    `json:"text" xml:"text" form:"text"`
}

type User struct {
	gorm.Model                // Adds some metadata fields to the table
	ID              uuid.UUID `gorm:"type:uuid"` // Explicitly specify the type to be uuid
	FullName        string    `json:"fullname" xml:"fullname" form:"fullname"`
	Email           string    `json:"email" xml:"email" form:"email"`
	Username        string    `gorm:"unique;not null" json:"username" xml:"username" form:"username"`
	Password        string    `json:"password" xml:"password" form:"password"`
	ConfirmPassword string    `gorm:"-" json:"confirmPassword" xml:"confirmPassword" form:"confirmPassword"`
	Role            string    `json:"role" xml:"role" form:"role"`
}

func (u User) CheckPasswordHash(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	return err == nil
}
